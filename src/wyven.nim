from engine/graphics/glfw4nim/glfw import nil
import engine/graphics/graphics as graphics

graphics.start()

block gameloop:
  while glfw.WindowShouldClose(graphics.window) == 0:

    glfw.SwapBuffers(graphics.window)

    glfw.PollEvents()

    if glfw.GetKey(graphics.window, glfw.KEY_ESCAPE) == 1:
      glfw.SetWindowShouldClose(graphics.window,1)

    #Test:
    graphics.draw()

graphics.terminate()
