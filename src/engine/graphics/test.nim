import vertex
import ../vector.nim

var vertices* = @[
  Vertex(pos: vec2(-0.5, -0.5), texCoord: vec2(8, 0)),
  Vertex(pos: vec2(0.5, -0.5), texCoord: vec2(0, 0)),
  Vertex(pos: vec2(0.5, 0.5), texCoord: vec2(0, 8)),
  Vertex(pos: vec2(-0.5, 0.5), texCoord: vec2(8, 8)) ]

var indices* = @[0'u16,1,2,2,3,0]
