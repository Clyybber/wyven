import vulkanim/vulkan
{.link: "/usr/lib/libvulkan.so".} #links vulkan
{.warning[UnusedImport]: off.}
from glfw4nim/glfw import nil

template data[T](sequence: openarray[T]): ptr T {.used.} =
  unsafeAddr sequence[0]

template check(result: VkResult) {.used.} =
  assert result == vkSuccess

using
  device: VkDevice
  instance: VkInstance
  physicalDevice: VkPhysicalDevice
  surface: VkSurfaceKHR
  swapchain: VkSwapchainKHR
  pipelineLayout: VkPipelineLayout
  pipeline: VkPipeline
  renderPass: VkRenderPass
  commandPool: VkCommandPool
