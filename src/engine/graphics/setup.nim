include helper
import debug
from sequtils import deduplicate

type QueueFamilies* = array[2, uint32] #1: Graphics, 2: Present

const requiredExtensions = [vkKHRSwapchainExtensionName]

proc createWindow*(resizeCallback: proc): glfw.Window =
  if glfw.Init() != glfw.True:
    quit "Failed to init GLFW"
  glfw.WindowHint(glfw.CLIENT_API, glfw.NO_API)
  result = glfw.CreateWindow(800, 600, "Wyven", nil, nil)
  if glfw.VulkanSupported() != glfw.True:
    quit "Vulkan not supported"
  discard glfw.SetFramebufferSizeCallback(result, resizeCallback)

proc getRequiredExtensions(): seq[string] =
  var glfwExtensionCount: uint32
  result.add(cstringArrayToSeq(cast[cstringArray](glfw.GetRequiredInstanceExtensions(addr glfwExtensionCount)), glfwExtensionCount))
  when enableValidationLayers:
    result.add(vkEXTDebugUtilsExtensionName)
  result = result.deduplicate()

proc createInstance*(): VkInstance =
  var appInfo = VkApplicationInfo( sType: vkStructureTypeApplicationInfo,
    pApplicationName: "Wyven",
    applicationVersion: vkMakeVersion(0,0,1),
    pEngineName: "WyvenEngine",
    engineVersion: vkMakeVersion(0,0,1),
    apiVersion: vkMakeVersion(1,0,2))
  when enableValidationLayers:
    assert(checkValidationLayerSupport(validationLayers))
  var requiredExtensions = getRequiredExtensions()
  var instanceCreateInfo = VkInstanceCreateInfo( sType: vkStructureTypeInstanceCreateInfo,
    flags: 0,
    pApplicationInfo: addr appInfo,
    ppEnabledLayerNames: allocCStringArray(validationLayers), enabledLayerCount: validationLayers.len.uint32,
    ppEnabledExtensionNames: allocCStringArray(requiredExtensions), enabledExtensionCount: requiredExtensions.len.uint32 )
  check: vkCreateInstance(addr instanceCreateInfo, nil, addr result)

proc createSurface*(instance; window: glfw.Window): VkSurfaceKHR =
  if vulkan.VkResult(glfw.CreateWindowSurface(cast[glfw.VkInstance](instance), window, nil, cast[ptr glfw.VkSurfaceKHR](addr result))) != vkSuccess:
    quit "Failed to create surface"

proc getSwapchainCapabilities*(physicalDevice, surface): VkSurfaceCapabilitiesKHR =
  check: physicalDevice.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(surface, addr result)

proc getSwapchainFormats*(physicalDevice, surface): seq[VkSurfaceFormatKHR] =
  var formatCount: uint32
  check: physicalDevice.vkGetPhysicalDeviceSurfaceFormatsKHR(surface, addr formatCount, nil)
  result.setLen(formatCount)
  check: physicalDevice.vkGetPhysicalDeviceSurfaceFormatsKHR(surface, addr formatCount, result.data)

proc getSwapchainPresentModes*(physicalDevice, surface): seq[VkPresentModeKHR] =
  var presentModeCount: uint32
  check: physicalDevice.vkGetPhysicalDeviceSurfacePresentModesKHR(surface, addr presentModeCount, nil)
  result.setLen(presentModeCount)
  check: physicalDevice.vkGetPhysicalDeviceSurfacePresentModesKHR(surface, addr presentModeCount, result.data)

proc deviceSupportsSwapchain(physicalDevice, surface): bool =
  var formats = physicalDevice.getSwapchainFormats(surface)
  var presentModes = physicalDevice.getSwapchainPresentModes(surface)
  return formats.len > 0 and presentModes.len > 0

proc deviceSupportsExtensions(physicalDevice): bool =
  var extensionCount: uint32
  check: physicalDevice.vkEnumerateDeviceExtensionProperties(nil, addr extensionCount, nil)
  var availableExtensions = newSeq[VkExtensionProperties](extensionCount)
  check: physicalDevice.vkEnumerateDeviceExtensionProperties(nil, addr extensioncount, availableExtensions.data)
  for requiredExtension in requiredExtensions:
    var extensionSupported = false
    for availableExtension in availableExtensions.mitems:
      if $cast[cstring](addr availableExtension) == requiredExtension:
        extensionSupported = true
    if not extensionSupported:
      return false
  return true

proc deviceSuitability(physicalDevice, surface): int =
  #var deviceProperties: VkPhysicalDeviceProperties
  #var deviceFeatures: VkPhysicalDeviceFeatures
  #physicalDevice.vkGetPhysicalDeviceProperties(addr deviceProperties)
  #physicalDevice.vkGetPhysicalDeviceFeatures(addr deviceFeatures)
  var queueFamilyCount: uint32
  physicalDevice.vkGetPhysicalDeviceQueueFamilyProperties(addr queueFamilyCount, nil)
  var queueFamilies = newSeq[VkQueueFamilyProperties](queueFamilyCount)
  physicalDevice.vkGetPhysicalDeviceQueueFamilyProperties(addr queueFamilyCount, queueFamilies.data)
  for queueFamilyIndex, queueFamily in queueFamilies:
    var supportsGraphics = (queueFamily.queueFlags and ord(vkQueueGraphicsBit)) != 0
    var supportsPresent: VkBool32
    check: physicalDevice.vkGetPhysicalDeviceSurfaceSupportKHR(queueFamilyIndex.uint32, surface, addr supportsPresent)
    var supportsExtensions = physicalDevice.deviceSupportsExtensions()
    if supportsPresent == vkTrue and supportsGraphics and supportsExtensions: #Perhaps add support for present and graphics in different queueFamilies
      result += 1
      break
  if result > 0:
    if physicalDevice.deviceSupportsSwapchain(surface):  #is this necessary?
      result += 10 #Result over 10, eg 11 indicates support for swapchain,present,graphics and extensions

proc pickPhysicalDevice*(instance, surface): VkPhysicalDevice =
  var physicalDeviceCount: uint32
  check: vkEnumeratePhysicalDevices(instance, addr physicalDeviceCount, nil)
  if physicalDeviceCount == 0:
    quit "No GPUs with vulkan support found"
  var physicalDevices = newSeq[VkPhysicalDevice](physicalDeviceCount)
  check: vkEnumeratePhysicalDevices(instance, addr physicalDeviceCount, physicalDevices.data)
  for physicalDevice in physicalDevices:
    if physicalDevice.deviceSuitability(surface) > 10: #TODO: Implement ordering and choosing the best
      return physicalDevice
  quit "No suitable GPUs found"

proc getQueueFamilies*(physicalDevice, surface): QueueFamilies = #maybe integrate into deviceSuitability?
  var queueFamilyCount: uint32
  physicalDevice.vkGetPhysicalDeviceQueueFamilyProperties(addr queueFamilyCount, nil)
  if queueFamilyCount == 0: #necessary?
    quit "Lost queue families. Something is REALLY wrong here"
  #queueFamily used here for queueFamilyProperty
  var queueFamilys = newSeq[VkQueueFamilyProperties](queueFamilyCount)
  physicalDevice.vkGetPhysicalDeviceQueueFamilyProperties(addr queueFamilyCount, queueFamilys.data)
  for queueFamilyIndex, queueFamily in queueFamilys:
    if (queueFamily.queueFlags and ord(vkQueueGraphicsBit)) != 0:
      result[0] = queueFamilyIndex.uint32
    var supportsPresent: VkBool32 = vkFalse
    check: physicalDevice.vkGetPhysicalDeviceSurfaceSupportKHR(queueFamilyIndex.uint32, surface, addr supportsPresent)
    if supportsPresent == vkTrue:
      result[1] = queueFamilyIndex.uint32

proc createLogicalDevice*(instance, physicalDevice; queueFamilies: QueueFamilies): VkDevice =
  var queuePriority = 1'f
  var alreadyIn: seq[uint32] #So we dont create duplicate queueCreateInfos
  var queueInfos: seq[VkDeviceQueueCreateInfo]
  for index in queueFamilies:
    if not alreadyIn.contains(index):
      alreadyIn.add(index)
      queueInfos.add(
        VkDeviceQueueCreateInfo( sType: vkStructureTypeDeviceQueueCreateInfo,
          queueFamilyIndex: index,
          queueCount: 1,
          pQueuePriorities: addr queuePriority))
  var deviceInfo = VkDeviceCreateInfo( sType: vkStructureTypeDeviceCreateInfo,
    pQueueCreateInfos: queueInfos.data, queueCreateInfoCount: queueInfos.len.uint32,
    ppEnabledExtensionNames: allocCStringArray(requiredExtensions), enabledExtensionCount: requiredExtensions.len.uint32 ) #What about glfw extensions?
  check: physicalDevice.vkCreateDevice(addr deviceInfo, nil, addr result)

proc getDeviceQueues*(device: VkDevice, queueFamilies: QueueFamilies): tuple[graphicQueue: VkQueue, presentQueue: VkQueue] =
  device.vkGetDeviceQueue(queueFamilies[0], 0, addr result.graphicQueue)
  device.vkGetDeviceQueue(queueFamilies[0], 0, addr result.presentQueue) #TODO: what if they are not the same?

