include helper

const enableProfiling* = true

proc createQueryPool*(device): VkQueryPool =
  var queryPoolInfo = VkQueryPoolCreateInfo( sType: vkStructureTypeQueryPoolCreateInfo,
    queryType: vkQueryTypeTimestamp,
    queryCount: 2 )
  check: vkCreateQueryPool(device, addr queryPoolInfo, nil, addr result)

