include helper #imports vulkan

import ../vector

#Stateless parts:
import vertex

import setup
import debug
import swapchain as swpchn
import renderpass as rndrpss
import descriptor
import pipeline as ppln
import profile
import shader
import command
import buffers
import sync
import image

import test

type UniformBufferObject = object
  color: Vec3

const maxFramesInFlight = 2

when enableValidationLayers:
  var messenger: VkDebugUtilsMessengerEXT

var
  #Statics:
  physicalDevice: VkPhysicalDevice
  window*: glfw.Window #Export this for the main loop
  instance: VkInstance
  surface: VkSurfaceKHR 
  device: VkDevice
  queueFamilies: QueueFamilies 
  #Available format stuff:
  swapchainCaps: VkSurfaceCapabilitiesKHR
  swapchainFormats: seq[VkSurfaceFormatKHR]
  swapchainPresentModes: seq[VkPresentModeKHR]
  #Selected format stuff:
  extent: VkExtent2D
  format: VkSurfaceFormatKHR
  presentMode: VkPresentModeKHR
  imageCount: uint32
  #Swapchain:
  swapchain: VkSwapchainKHR
  swapchainImages: seq[VkImage]
  swapchainImageViews: seq[VkImageView]
  #Attachment stuff:
  renderPass: VkRenderPass
  framebuffers: seq[VkFramebuffer]
  descriptorSetLayout: VkDescriptorSetLayout
  descriptorPool: VkDescriptorPool
  descriptorSets: seq[VkDescriptorSet]
  pipelineLayout: VkPipelineLayout
  pipeline: VkPipeline
  #Buffers:
  vertexBuffer, indexBuffer: VkBuffer
  indexBufferMemory, vertexBufferMemory: VkDeviceMemory
  #One for every swapchain image
  uniformBuffers: seq[VkBuffer]
  uniformBuffersMemory: seq[VkDeviceMemory]
  uniformBuffersRAM: seq[UniformBufferObject]
  #Command stuff:
  graphicQueue, presentQueue: VkQueue
  commandPool: VkCommandPool
  commandBuffers: seq[VkCommandBuffer]
  #Sync stuff:
  imageAvailableSemaphores, renderFinishedSemaphores: array[maxFramesInFlight, VkSemaphore]
  inFlightFences: array[maxFramesInFlight, VkFence]

  #ShaderModules
  vertShaderModule, fragShaderModule: VkShaderModule

  #Profiling
  queryPool: VkQueryPool

  framebufferResized = false

  currentFrameIndex: int8

  #Profiling stuff:
  count = 0
  totalTime = 0'd

  #Texture test
  textureImage: VkImage
  textureImageMemory: VkDeviceMemory
  textureImageView: VkImageView
  textureSampler: VkSampler

proc resizeCallback(glfwWindow: pointer, width, height: cint) {.cdecl.} =
  framebufferResized = true

proc start* =
  window                 = createWindow(resizeCallback)
  instance               = createInstance()
  when enableValidationLayers:
    messenger            = setupVulkanDebugCallback(instance)
  surface                = createSurface(instance, window)
  physicalDevice         = pickPhysicalDevice(instance, surface)
  #Queue stuff
  queueFamilies          = physicalDevice.getQueueFamilies(surface)
  device                 = createLogicalDevice(instance, physicalDevice, queueFamilies)
  (graphicQueue,
   presentQueue)         = device.getDeviceQueues(queueFamilies)
  #Format stuff
  swapchainCaps          = physicalDevice.getSwapchainCapabilities(surface)
  swapchainFormats       = physicalDevice.getSwapchainFormats(surface)
  swapchainPresentModes  = physicalDevice.getSwapchainPresentModes(surface)
  format                 = pickFormat(swapchainFormats)
  presentMode            = pickPresentMode(swapchainPresentModes)
  extent                 = pickExtent(swapchainCaps, window)
  let minImageCount      = pickSwapImageCount(swapchainCaps)
  #Swapchain stuff
  swapchain              = device.createSwapchain(surface, swapchainCaps, format, presentMode, extent, minImageCount)
  swapchainImages        = device.getSwapchainImages(swapchain)
  swapchainImageViews    = device.createImageViews(swapchainImages, format.format) #Maybe whole format?
  imageCount             = swapchainImages.len.uint32
  #Renderpass stuff
  renderPass             = device.createRenderPass(format.format)
  descriptorSetLayout    = device.createDescriptorSetLayout()
  #Shader modules:
  vertShaderModule       = device.createShaderModule("default.vert.spv")
  fragShaderModule       = device.createShaderModule("default.frag.spv")
  #Pipeline stuff
  pipelineLayout         = device.createGraphicsPipelineLayout(@[descriptorSetLayout])
  pipeline               = device.createGraphicsPipeline(pipelineLayout, renderPass, extent, vertShaderModule, fragShaderModule)
  #Profiling (doesn't matter much where we put this :)
  when enableProfiling:
    queryPool            = device.createQueryPool()
  framebuffers           = device.createFramebuffers(renderPass, extent, swapchainImageViews)
  commandPool            = device.createCommandPool(queueFamilies)
  #Vertex buffer
  (vertexBuffer,
   vertexBufferMemory)   = device.createVertexBuffer(physicalDevice, commandPool, graphicQueue, vertices)
  #Index buffer
  (indexBuffer,
   indexBufferMemory)    = device.createIndexBuffer(physicalDevice, commandPool, graphicQueue, indices)
  #Uniform buffer
  (uniformBuffers,
   uniformBuffersMemory) = device.createUniformBuffers(physicalDevice, Vertex.sizeof, imageCount.int)
  uniformBuffersRAM.setLen(imageCount.int)
  for UBO in uniformBuffersRAM.mitems:
    UBO = UniformBufferObject(
      color: vec3(100, 100, 100) )
  #Descriptor stuff
  descriptorPool         = device.createDescriptorPool(imageCount)
  #Texture stuff
  (textureImage,
   textureImageMemory)   = device.createTextureImage(physicalDevice, commandPool, graphicQueue)
  textureImageView       = device.createTextureImageView(textureImage)
  textureSampler         = device.createTextureSampler()
  descriptorSets         = device.createDescriptorSets(descriptorSetLayout, descriptorPool, uniformBuffers, textureImageView, textureSampler, imageCount.int)
  commandBuffers         = device.createCommandBuffers(commandPool, renderPass, pipeline, pipelineLayout, extent, framebuffers, descriptorSets, vertexBuffer, indexBuffer, queryPool) #Querypool is optionool
  #Sync stuff
  for i in 0..<maxFramesInFlight:
    imageAvailableSemaphores[i] = device.createSemaphore()
    renderFinishedSemaphores[i] = device.createSemaphore()
    inFlightFences[i]           = device.createFence()

proc terminate* =
  discard device.vkDeviceWaitIdle()
  when enableProfiling:
    device.vkDestroyQueryPool(queryPool, nil)
  for framebuffer in framebuffers:
    device.vkDestroyFramebuffer(framebuffer, nil)
  #Shader modules
  device.vkDestroyShaderModule(vertShaderModule, nil)
  device.vkDestroyShaderModule(fragShaderModule, nil)
  device.vkDestroyPipeline(pipeline, nil)
  device.vkDestroyPipelineLayout(pipelineLayout, nil)
  device.vkDestroyRenderPass(renderPass, nil)
  for swapchainImageView in swapchainImageViews:
    device.vkDestroyImageView(swapchainImageView, nil)
  device.vkDestroySwapchainKHR(swapchain, nil)
  device.vkDestroyDescriptorPool(descriptorPool, nil)
  device.vkDestroyDescriptorSetLayout(descriptorSetLayout, nil)
  device.vkDestroySampler(textureSampler, nil)
  device.vkDestroyImageView(textureImageView, nil)
  device.vkDestroyImage(textureImage, nil)
  device.vkFreeMemory(textureImageMemory, nil)
  for i in 0..<swapchainImages.len:
    device.vkDestroyBuffer(uniformBuffers[i], nil)
    device.vkFreeMemory(uniformBuffersMemory[i], nil)
  device.vkDestroyBuffer(indexBuffer, nil)
  device.vkFreeMemory(indexBufferMemory, nil)
  device.vkDestroyBuffer(vertexBuffer, nil)
  device.vkFreeMemory(vertexBufferMemory, nil)
  for frameIndex in 0..<maxFramesInFlight:
    device.vkDestroySemaphore(renderFinishedSemaphores[frameIndex], nil)
    device.vkDestroySemaphore(imageAvailableSemaphores[frameIndex], nil)
    device.vkDestroyFence(inFlightFences[frameIndex], nil)
  device.vkDestroyCommandPool(commandPool, nil)
  device.vkDestroyDevice(nil)
  when enableValidationLayers:
    destroyVulkanDebugCallback(instance, messenger)
  vkDestroySurfaceKHR(instance, surface, nil)
  vkDestroyInstance(instance, nil)
  glfw.DestroyWindow(window)
  glfw.Terminate()

proc recreateSwapchain =
  check: device.vkDeviceWaitIdle()
  for framebuffer in framebuffers:
    device.vkDestroyFramebuffer(framebuffer, nil)
  device.vkFreeCommandBuffers(commandPool, commandBuffers.len.uint32, commandBuffers.data) #We reuse the existing command buffer
  device.vkDestroyPipeline(pipeline, nil)
  device.vkDestroyRenderPass(renderPass, nil)
  for swapchainImageView in swapchainImageViews:
    device.vkDestroyImageView(swapchainImageView, nil)
  device.vkDestroySwapchainKHR(swapchain, nil)
  swapchain           = device.createSwapchain(surface, swapchainCaps, format, presentMode, extent, imageCount)
  swapchainImages     = device.getSwapchainImages(swapchain)
  swapchainImageViews = device.createImageViews(swapchainImages, format.format)
  renderPass          = device.createRenderPass(format.format)
  pipeline            = device.createGraphicsPipeline(pipelineLayout, renderPass, extent, vertShaderModule, fragShaderModule)
  framebuffers        = device.createFramebuffers(renderPass, extent, swapchainImageViews)
  commandBuffers      = device.createCommandBuffers(commandPool, renderPass, pipeline, pipelineLayout, extent, framebuffers, descriptorSets, vertexBuffer, indexBuffer, queryPool) #Querypool is optionool

proc draw* =
  discard device.vkWaitForFences(1, addr inFlightFences[currentFrameIndex], vkTrue, uint64.high)
  var imageIndex: uint32
  #Acquire new image from the swapchain
  var acquireImageResult = device.vkAcquireNextImageKHR(swapChain, uint64.high, imageAvailableSemaphores[currentFrameIndex], vkNullHandle, addr imageIndex)
  if acquireImageResult == vkErrorOutOfDateKhr: #When does this actually happen??
    debugEcho "vkErrorOutOfDateKhr"
    #Resize swapchain
    swapchainCaps = physicalDevice.getSwapchainCapabilities(surface)
    extent = pickExtent(swapchainCaps, window)
    recreateSwapchain()
    return
  elif acquireImageResult != vkSuccess and acquireImageResult != vkSuboptimalKhr:
    quit "Failed to acquire swapchain image"
  var waitSemaphores = [imageAvailableSemaphores[currentFrameIndex]]  #array not needed it count = 1
  var waitStages = [vkPipelineStageColorAttachmentOutputBit.uint32]
  var signalSemaphores = [renderFinishedSemaphores[currentFrameIndex]]
  device.updateUniformBuffer(uniformBuffersMemory[imageIndex.int], uniformBuffersRAM[imageIndex.int])
  var submitInfo = VkSubmitInfo( sType: vkStructureTypeSubmitInfo,
    pWaitSemaphores: waitSemaphores.data, waitSemaphoreCount: waitSemaphores.len.uint32,
    pWaitDstStageMask: waitStages.data,
    pCommandBuffers: addr commandBuffers[imageIndex.int], commandBufferCount: 1,
    pSignalSemaphores: signalSemaphores.data, signalSemaphoreCount: signalSemaphores.len.uint32 )
  discard device.vkResetFences(1, addr inFlightFences[currentFrameIndex])
  check: vkQueueSubmit(graphicQueue, 1, addr submitInfo, inFlightFences[currentFrameIndex])
  var swapchains = [swapchain]
  var presentInfo = VkPresentInfoKHR( sType: vkStructureTypePresentInfoKHR,
    pWaitSemaphores: signalSemaphores.data, waitSemaphoreCount: signalSemaphores.len.uint32,
    pSwapchains: swapchains.data, swapchainCount: 1,
    pImageIndices: addr imageIndex )
  #Present queue
  var queuePresentResult = vkQueuePresentKHR(presentQueue, addr presentInfo)
  if queuePresentResult == vkErrorOutOfDateKhr or queuePresentResult == vkSuboptimalKhr or framebufferResized:
    framebufferResized = false
    #Resize swapchain
    swapchainCaps = physicalDevice.getSwapchainCapabilities(surface)
    extent = pickExtent(swapchainCaps, window)
    recreateSwapchain()
  elif queuePresentResult != vkSuccess:
    quit "Failed to present swapchain image"
  currentFrameIndex = (currentFrameIndex + 1) mod maxFramesInFlight
  when enableProfiling:
    var begin = 0'u32
    var ending = 0'u32

    count += 1
    const period = 5000 #properties.limits.timestampPeriod
    if count >= period:
      echo "VK Render Time (avg of past ",period," frames): ", $(totalTime / period)
      count = 0
      totalTime = 0

    check: device.vkGetQueryPoolResults(queryPool, 1, 1, sizeof uint32, addr ending, 0, vkQueryResultWaitBit)
    check: device.vkGetQueryPoolResults(queryPool, 0, 1, sizeof uint32, addr begin, 0, vkQueryResultWaitBit)
    totalTime += float(ending - begin) / 1e6
