import math

type Vec3* = object
  x*, y*, z*: float32

proc vec3*(x, y, z: float32): Vec3 {.inline.} =
  Vec3(x: x, y: y, z: z)

proc `-`*(a, b: Vec3): Vec3 {.inline.} =
  vec3(a.x - b.x, a.y - b.y, a.z - b.z)

proc `*`*(v: Vec3, s: float32): Vec3 {.inline.} =
  vec3(v.x * s, v.y * s, v.z * s)

proc dot*(a, b: Vec3): float32 {.inline.} =
  a.x * b.x + a.y * b.y + a.z * b.z

proc cross*(a, b: Vec3): Vec3 {.inline.} =
  vec3(
    a.y * b.z - a.z * b.y,
    a.z * b.x - a.x * b.z,
    a.x * b.y - a.y * b.x)

proc len*(v: Vec3): float32 {.inline.} =
  sqrt(v.x * v.x + v.y * v.y + v.z * v.z)

proc normalize*(v: Vec3): Vec3 {.inline.} =
  let len = v.len
  vec3(v.x / len, v.y / len, v.z / len)

type Vec2* = object
  x*, y*: float32

proc vec2*(x, y: float32): Vec2 {.inline.} =
  Vec2(x: x, y: y)

proc `-`*(a, b: Vec2): Vec2 {.inline.} =
  vec2(a.x - b.x, a.y - b.y)

proc `*`*(v: Vec2, s: float32): Vec2 {.inline.} =
  vec2(v.x * s, v.y * s)

proc dot*(a, b: Vec2): float32 {.inline.} =
  a.x * b.x + a.y * b.y

proc cross*(a, b: Vec2): float32 {.inline.} =
  a.x * b.y - a.y * b.x

proc len*(v: Vec2): float32 {.inline.} =
  sqrt(v.x * v.x + v.y * v.y)

proc normalize*(v: Vec2): Vec2 {.inline.} =
  let len = v.len
  vec2(v.x / len, v.y / len)
